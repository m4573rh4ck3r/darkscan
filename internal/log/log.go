package log

import (
	"fmt"
	"log"
	"os"
)

var (
	colors map[string]string = map[string]string{
		"reset":  "\033[0m",
		"red":    "\033[0;31m",
		"yellow": "\033[0;33m",
		"cyan":   "\033[0;36m",
	}
)

func NewDebugLogger(noColor bool) *log.Logger {
	var format = ""
	if !noColor {
		format = fmt.Sprintf("[%sDEBUG%s] ", colors["cyan"], colors["reset"])
	} else {
		format = fmt.Sprint("[DEBUG] ")
	}
	return log.New(os.Stdout, format, log.Lshortfile)
}

func NewPrintLogger(noColor bool) *log.Logger {
	return log.New(os.Stdout, "", log.Lmsgprefix)
}

func NewInfoLogger(noColor bool) *log.Logger {
	var format = ""
	if !noColor {
		format = fmt.Sprintf("[%sINFO%s] ", colors["cyan"], colors["reset"])
	} else {
		format = fmt.Sprint("[INFO] ")
	}
	return log.New(os.Stdout, format, log.Lmsgprefix)
}

func NewWarningLogger(noColor bool) *log.Logger {
	var format = ""
	if !noColor {
		format = fmt.Sprintf("[%sWARNING%s] ", colors["yellow"], colors["reset"])
	} else {
		format = fmt.Sprint("[WARNING] ")
	}
	return log.New(os.Stderr, format, log.Lshortfile)
}

func NewErrorLogger(noColor bool) *log.Logger {
	var format = ""
	if !noColor {
		format = fmt.Sprintf("[%sERROR%s] ", colors["red"], colors["reset"])
	} else {
		format = fmt.Sprint("[ERROR] ")
	}
	return log.New(os.Stderr, format, log.Lshortfile)
}

func NewFatalLogger(noColor bool) *log.Logger {
	var format = ""
	if !noColor {
		format = fmt.Sprintf("[%sFATAL%s] ", colors["red"], colors["reset"])
	} else {
		format = fmt.Sprint("[FATAL] ")
	}
	return log.New(os.Stderr, format, log.Lshortfile)
}

func NewPanicLogger(noColor bool) *log.Logger {
	var format = ""
	if !noColor {
		format = fmt.Sprintf("[%sPANIC%s] ", colors["red"], colors["reset"])
	} else {
		format = fmt.Sprint("[PANIC] ")
	}
	return log.New(os.Stderr, format, log.Lshortfile)
}
