package tor

import (
	"context"
	"encoding/base64"
	"io/ioutil"
	"net/http"
	"net/url"
	"regexp"
	"strings"
	"time"

	"github.com/spf13/viper"

	"gitlab.com/m4573rh4ck3r/darkscan/internal/log"
)

func crawl(ctx context.Context, addr string, logFile string) (*SiteData, error) {
	for {
		select {
		case <-ctx.Done():
			return nil, nil
		default:
			addr = strings.TrimRight(addr, "/")
			proxy, err := url.Parse("socks5://127.0.0.1:9050")
			if err != nil {
				return nil, err
			}

			if !regexp.MustCompile(`[a-z]*://.*`).MatchString(addr) {
				log.NewInfoLogger(viper.GetBool("no-color")).Println("protocol scheme is empty. Using http")
				addr = "http://" + addr
			}
			log.NewDebugLogger(viper.GetBool("no-color")).Printf("sending get request to %s\n", addr)
			c := &http.Client{
				Transport: &http.Transport{Proxy: http.ProxyURL(proxy)},
				Timeout:   time.Second * 20,
			}

			resp, err := c.Get(addr)
			if err != nil {
				return nil, err
			}

			html, err := ioutil.ReadAll(resp.Body)
			if err != nil {
				return nil, err
			}
			title := stripHtmlTags(getTitleByRegex(string(html)))
			log.NewPrintLogger(viper.GetBool("no-color")).Printf("%s: %s\n", addr, title)

			encoding := base64.NewEncoding("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789=/")
			encodedHtml := encoding.EncodeToString(html)

			s := &SiteData{
				Title: title,
				URL:   addr,
				Html:  string(encodedHtml),
			}
			return s, err
		}
	}
}

func getTitleByRegex(html string) string {
	return regexp.MustCompile(`<title>.*</title>`).FindString(html)
}

func stripHtmlTags(html string) string {
	return regexp.MustCompile(`(</?[a-zA-Z]+?[^>]*/?>)*`).ReplaceAllString(html, "")
}
