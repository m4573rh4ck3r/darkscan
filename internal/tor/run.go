package tor

import (
	"context"
	"encoding/json"
	"fmt"
	"os"

	"github.com/spf13/viper"

	"gitlab.com/m4573rh4ck3r/darkscan/internal/log"
	"gitlab.com/m4573rh4ck3r/darkscan/internal/onion"
)

func Run(ctx context.Context, random bool, parallel int64, version int64, logFile string, addrs ...string) error {
	ch := make(chan string)

	if random {
		go func() {
			if version == 2 {
				go func() {
					onion.GenV2Addresses(ctx, ch, parallel)
				}()
			} else if version == 3 {
				go func() {
					onion.GenV3Addresses(ctx, ch, parallel)
				}()
			}
		}()

		doneChan := make(chan bool)
		defer close(doneChan)
		for i := int64(0); i < parallel; i++ {
			go func() {
				addr, ok := <-ch
				if !ok {
					return
				}
				siteData, err := crawl(ctx, addr, logFile)
				if err != nil {
					log.NewWarningLogger(viper.GetBool("no-color")).Println(err)
				}
				if siteData != nil {
					if err := logSiteData(logFile, siteData); err != nil {
						log.NewWarningLogger(viper.GetBool("no-color")).Println(err)
					}
				}

				doneChan <- true
			}()
		}

		for {
			select {
			case _, ok := <-doneChan:
				if !ok {
					return nil
				}
				go func() {
					addr, ok := <-ch
					if !ok {
						return
					}
					siteData, err := crawl(ctx, addr, logFile)
					if err != nil {
						log.NewWarningLogger(viper.GetBool("no-color")).Println(err)
					}
					if siteData != nil {
						if err := logSiteData(logFile, siteData); err != nil {
							log.NewWarningLogger(viper.GetBool("no-color")).Println(err)
						}
					}
					doneChan <- true
				}()
			case <-ctx.Done():
				return nil
			default:
			}
		}

		// neither random mode nor explicit addresses were provided so there is nothing to do
	} else {
		if addrs == nil {
			return fmt.Errorf("no addresses were provided")
		}
		for _, addr := range addrs {
			siteData, err := crawl(ctx, addr, logFile)
			if err != nil {
				log.NewWarningLogger(viper.GetBool("no-color")).Println(err)
			}
			if siteData != nil {
				if err := logSiteData(logFile, siteData); err != nil {
					log.NewWarningLogger(viper.GetBool("no-color")).Println(err)
				}
			}
		}
	}

	return nil
}

func logSiteData(logFile string, s *SiteData) error {
	b, err := json.Marshal(s)
	if err != nil {
		return err
	}
	w, err := os.OpenFile(logFile, os.O_RDWR|os.O_APPEND|os.O_CREATE, 0644)
	if err != nil {
		return err
	}
	b = append(b, []byte("\n")...)
	_, err = w.Write(b)
	return err
}
