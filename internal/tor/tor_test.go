package tor

import (
	"context"
	"testing"
	"time"
)

func TestCrawl(t *testing.T) {
	ctx, cancel := context.WithCancel(context.Background())
	go func() {
		time.Sleep(time.Second * 20)
		cancel()
	}()
	if _, err := crawl(ctx, "http://duckduckgo.com", "darkscan_test.log"); err != nil {
		t.Fatal(err)
	}
}

func TestNonRandomRun(t *testing.T) {
	ctx := context.Background()
	if err := Run(ctx, false, 1, 0, "darkscan_test.log", "http://duckduckgo.com"); err != nil {
		t.Fatal(err)
	}
}

func TestRandomV3Run(t *testing.T) {
	ctx := context.Background()
	ctx, cancel := context.WithCancel(ctx)
	timeChan := time.After(time.Second * 10)
	go func() {
		for {
			select {
			case <-timeChan:
				cancel()
			default:
			}
		}
	}()
	if err := Run(ctx, true, 1, int64(3), "darkscan_test.log"); err != nil {
		t.Fatal(err)
	}
}
