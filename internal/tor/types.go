package tor

type SiteData struct {
	Title string
	Html  string
	URL   string
}
