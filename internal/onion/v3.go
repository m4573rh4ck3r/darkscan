package onion

import (
	"context"
	"crypto/rand"
	"encoding/base32"
	"strings"

	"github.com/spf13/viper"
	"golang.org/x/crypto/ed25519"
	"golang.org/x/crypto/sha3"

	"gitlab.com/m4573rh4ck3r/darkscan/internal/log"
)

// Hidden service version
const version = byte(0x03)

// Salt used to create checkdigits
const salt = ".onion checksum"

func getCheckdigits(pub ed25519.PublicKey) []byte {
	// Calculate checksum sha3(".onion checksum" || publicKey || version)
	checkstr := []byte(salt)
	checkstr = append(checkstr, pub...)
	checkstr = append(checkstr, version)
	checksum := sha3.Sum256(checkstr)
	return checksum[:2]
}

func GenV3Addresses(ctx context.Context, ch chan string, parallel int64) {
	doneChan := make(chan bool)
	for i := int64(0); i < parallel; i++ {
		go func() {
			v3worker(doneChan, ch)
		}()
	}
	for {
		select {
		case _, ok := <-doneChan:
			if !ok {
				break
			}
			go func() {
				v3worker(doneChan, ch)
			}()
		default:
		}
	}
}

func v3worker(doneChan chan bool, ch chan string) {
	pub, _, err := ed25519.GenerateKey(rand.Reader)
	if err != nil {
		log.NewFatalLogger(viper.GetBool("no-color")).Fatal(err)
	}
	checkdigits := getCheckdigits(pub)
	combined := pub[:]
	combined = append(combined, checkdigits...)
	combined = append(combined, version)
	serviceID := base32.StdEncoding.EncodeToString(combined)
	ch <- strings.ToLower(serviceID) + ".onion"
	doneChan <- true
}
