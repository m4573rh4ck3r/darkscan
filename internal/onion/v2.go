package onion

import (
	"context"
	"crypto/rand"
	"crypto/rsa"
	"crypto/sha1"
	"encoding/base32"
	"strings"

	"github.com/spf13/viper"

	"gitlab.com/m4573rh4ck3r/darkscan/internal/log"
)

var encoding = base32.NewEncoding("abcdefghijklmnopqrstuvwxyz234567")

func GenV2Addresses(ctx context.Context, ch chan string, parallel int64) {
	doneChan := make(chan bool)
	for i := int64(0); i < parallel; i++ {
		go func() {
			v2worker(doneChan, ch)
		}()
	}
	for {
		select {
		case _, ok := <-doneChan:
			if !ok {
				return
			}
			go func() {
				v2worker(doneChan, ch)
			}()
		default:
		}
	}
}

func v2worker(doneChan chan bool, ch chan string) {
	priv, err := rsa.GenerateKey(rand.Reader, 1024)
	if err != nil {
		log.NewFatalLogger(viper.GetBool("no-color")).Fatal(err)
	}

	sum := sha1.Sum(priv.PublicKey.N.Bytes())

	var strsum = ""

	for _, b := range sum {
		strsum += string(b)
	}

	encodedAddr := encoding.EncodeToString([]byte(strsum))
	chars := strings.Split(encodedAddr, "")

	var addr = ""
	for i := 0; i < 16; i++ {
		addr += chars[i]
	}

	ch <- addr + ".onion"
	doneChan <- true
}
