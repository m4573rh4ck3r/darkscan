package onion

import (
	"context"
	"testing"
)

func TestGenV3Addresses(t *testing.T) {
	ctx := context.Background()
	ch := make(chan string)

	go func() {
		GenV3Addresses(ctx, ch, 2)
	}()

	for i := 0; i < 3; i++ {
		addr, ok := <-ch
		if !ok {
			break
		}
		t.Logf("addr: %s\n", addr)
		if len(addr) != 56+6 {
			t.Fatalf("expected address length to be 56. Actual: %d\n", len(addr)-6)
		}
	}
}

func TestGenV2Addresses(t *testing.T) {
	ctx := context.Background()
	ch := make(chan string)

	go func() {
		GenV2Addresses(ctx, ch, 2)
	}()

	for i := 0; i < 3; i++ {
		addr, ok := <-ch
		if !ok {
			break
		}
		t.Logf("addr: %s\n", addr)
		if len(addr) != 16+6 {
			t.Fatalf("expected address length to be 16. Actual: %d\n", len(addr)-6)
		}
	}
}
