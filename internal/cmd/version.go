package cmd

import (
	"github.com/spf13/cobra"

	"gitlab.com/m4573rh4ck3r/darkscan/internal/log"
	"gitlab.com/m4573rh4ck3r/darkscan/internal/version"
)

func init() {
	rootCmd.AddCommand(versionCmd)
}

var versionCmd = &cobra.Command{
	Use:   "version",
	Short: "show detailed version output",
	Args:  cobra.NoArgs,
	Run: func(cmd *cobra.Command, args []string) {
		log.NewPrintLogger(noColor).Printf(version.DetailedVersionTemplate, rootCmd.Use, version.Version, version.GitRevision, version.GitBranch, version.GoVersion, version.BuildTime, version.OsArch)
	},
}
