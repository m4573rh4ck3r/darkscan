package cmd

import (
	"os"
	"path/filepath"

	"github.com/spf13/cobra"
	"golang.org/x/net/context"

	"gitlab.com/m4573rh4ck3r/darkscan/internal/log"
	"gitlab.com/m4573rh4ck3r/darkscan/internal/tor"
	"gitlab.com/m4573rh4ck3r/darkscan/internal/version"
)

var (
	verbosity int64
	noColor   bool
	parallel  int64
	logFile   string
)

func init() {
	rootCmd.PersistentFlags().Int64VarP(&verbosity, "verbose", "v", 2, "verbosity (0 = errors only, 1 = warning, 2 = normal output, 3 = info output, 4 = debug)")
	rootCmd.PersistentFlags().BoolVar(&noColor, "no-color", false, "disable colorized output")
	rootCmd.PersistentFlags().Int64VarP(&parallel, "parallel", "p", int64(10), "use randomly generated onion addresses")
	rootCmd.PersistentFlags().StringVarP(&logFile, "log-file", "l", "darkscan.log", "log file for successfully crawled addresses")
}

var rootCmd = &cobra.Command{
	Use:     "darkscan",
	Short:   "Darkscan is a command line tool to curl websites via the tor protocol",
	Args:    cobra.MinimumNArgs(1),
	Version: version.Version,
	Run: func(cmd *cobra.Command, args []string) {
		ctx, cancel := context.WithCancel(context.Background())
		defer cancel()
		logFileWithAbsPath, err := filepath.Abs(logFile)
		if err != nil {
			log.NewFatalLogger(noColor).Fatal(err)
		}
		logFileAbsDir := filepath.Dir(logFileWithAbsPath)
		os.MkdirAll(logFileAbsDir, 0755)
		if err := tor.Run(ctx, false, parallel, 0, logFileWithAbsPath, args...); err != nil {
			log.NewFatalLogger(noColor).Fatal(err)
		}
	},
}

func Execute() {
	if err := rootCmd.Execute(); err != nil {
		log.NewFatalLogger(noColor).Fatal(err)
	}
}
