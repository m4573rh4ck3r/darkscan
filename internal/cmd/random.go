package cmd

import (
	"strconv"

	"github.com/spf13/cobra"
	"golang.org/x/net/context"

	"gitlab.com/m4573rh4ck3r/darkscan/internal/log"
	"gitlab.com/m4573rh4ck3r/darkscan/internal/tor"
	"gitlab.com/m4573rh4ck3r/darkscan/internal/version"
)

func init() {
	rootCmd.AddCommand(randomCmd)
}

var randomCmd = &cobra.Command{
	Use:   "random",
	Short: "Darkscan is a command line tool to curl websites via the tor protocol",
	Aliases: []string{
		"r",
		"rand",
	},
	Args: cobra.ExactArgs(1),
	ValidArgs: []string{
		"2",
		"3",
	},
	Version: version.Version,
	Run: func(cmd *cobra.Command, args []string) {
		ctx, cancel := context.WithCancel(context.Background())
		defer cancel()
		v, err := strconv.Atoi(args[0])
		if err != nil {
			log.NewFatalLogger(noColor).Fatal(err)
		}
		if err := tor.Run(ctx, true, parallel, int64(v), logFile); err != nil {
			log.NewFatalLogger(noColor).Fatal(err)
		}
	},
}
