module gitlab.com/m4573rh4ck3r/darkscan

go 1.16

require (
	github.com/spf13/cobra v1.8.0
	github.com/spf13/viper v1.18.2
	golang.org/x/crypto v0.16.0
	golang.org/x/net v0.19.0
)
